import React from 'react';
import Select from 'react-select'
import axios from 'axios';

import country from "world-map-country-shapes";
import Grid from '@material-ui/core/Grid';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import withStyles from "@material-ui/core/styles/withStyles";
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import NumberFormat from 'react-number-format';

const useStyles = {
  root: {
    minWidth: 275,
  },
  title: {
    fontSize: 16,
    color:'#000'
  },
  Mundo: {
    paddingTop:'15px'
  },
  subTitle: {
    fontFamily:'Roboto',
    fontSize: 20,
    fontWeight:'700',
    color:'#BA0000'
  },

};



class App extends React.Component {
  constructor(props) {
    super(props);
  
    this.state = {  Pais:'CL',
                    SelectPais:[],
                    PaisSelected:{value: "CL", label: "Chile"},
                    Confirmados:0,
                    Recuperados:0,
                    Muertes:0,
                    ConfirmadosMundial:0,
                    RecuperadosMundial:0,
                    MuertesMundial:0,
                    FechaActualizacion:''};
    this.CambiarPais = this.CambiarPais.bind(this);
    this.onChangeSelect = this.onChangeSelect.bind(this);
    this.CambiarPais = this.CambiarPais.bind(this);
  }
  CambiarPais(Pais){
    let self = this;
    let Eleccion = this.state.SelectPais.filter(PaisFiltro => PaisFiltro.value == Pais)
    console.log(Pais)
    this.setState({Pais:Pais,PaisSelected:Eleccion})
    axios.get(`https://covid19.mathdro.id/api/countries/${Pais}`)
      .then(res => {
            console.log(res);
            if(res.status == 200){
              self.setState({ Confirmados:res.data.confirmed.value,
                              Recuperados:res.data.recovered.value,
                              Muertes:res.data.deaths.value})
            }
        
        })
  }
  componentDidMount(){
    let self  =this;
    axios.get(`https://covid19.mathdro.id/api/countries`)
      .then(res => {
        this.setState({SelectPais:res.data.countries.map(function(item,i){
                        return {value:item.iso2,label:item.name};
                      })
        })
      })
    axios.get(`https://covid19.mathdro.id/api/countries/CL`)
      .then(res => {
            console.log(res);
            if(res.status == 200){
              self.setState({ Confirmados:res.data.confirmed.value,
                              Recuperados:res.data.recovered.value,
                              Muertes:res.data.deaths.value})
            }
        
        })
    axios.get(`https://covid19.mathdro.id/api`)
      .then(res => {
            console.log(res);
            if(res.status == 200){
              self.setState({ ConfirmadosMundial:res.data.confirmed.value,
                              RecuperadosMundial:res.data.recovered.value,
                              MuertesMundial:res.data.deaths.value,
                              FechaActualizacion:res.data.lastUpdate})
            }
        
        })
        
  }
  onChangeSelect(Selected) {
    let self = this;
    if(Selected){
      this.setState({PaisSelected:Selected,Pais:Selected.value})
      axios.get(`https://covid19.mathdro.id/api/countries/${Selected.value}`)
      .then(res => {
            console.log(res);
            if(res.status == 200){
              self.setState({ Confirmados:res.data.confirmed.value,
                              Recuperados:res.data.recovered.value,
                              Muertes:res.data.deaths.value})
            }
        
        })
    }else{
      this.setState({PaisSelected:{},Pais:''})
    }
  }
  render(){
    const { classes } = this.props;
    return (
      <Grid
        container
        direction="row"
        justify="center"
        alignItems="stretch"
      >
        <AppBar position="static">
          <Toolbar>
            <Typography variant="h6" >
              CoronaVirus - Covid-19
            </Typography>
          </Toolbar>
        </AppBar>
        <Grid
          container
          direction="row"
          justify="center"
          alignItems="stretch"
        >
          <Grid item xs={12} md={4} lg={4}> 
            <Map Pais={this.state.Pais} CambiarPais={this.CambiarPais} key={this.state.Pais}/>
          </Grid>
          <Grid item xs={12} md={4} lg={4} style={{padding:'10px'}}> 
            <Select options={this.state.SelectPais}
                    onChange={this.onChangeSelect}
                    value={this.state.PaisSelected}
                    isSearchable
             />
          </Grid>
          <Grid
            container
            direction="row"
            justify="center"
            alignItems="stretch"

          >
            <Grid item xs={12} md={4} lg={4} style={{padding:'10px'}}> 
              <Card className={classes.root} variant="outlined">
                <CardContent>
                  <Typography className={classes.title} color="textSecondary" gutterBottom>
                    Confirmados
                  </Typography>
                  <Typography className={classes.subTitle} color="textSecondary" gutterBottom>
                    {this.state.Confirmados}
                  </Typography>
                </CardContent>
              </Card>
            </Grid>
            <Grid item xs={12} md={4} lg={4} style={{padding:'10px'}}> 
              <Card className={classes.root} variant="outlined">
                <CardContent>
                  <Typography className={classes.title} color="textSecondary" gutterBottom>
                    Recuperados
                  </Typography>
                  <Typography className={classes.subTitle} color="textSecondary" gutterBottom>
                    {this.state.Recuperados}
                  </Typography>
                </CardContent>
              </Card>
            </Grid>
            <Grid item xs={12} md={4} lg={4} style={{padding:'10px'}}> 
              <Card className={classes.root} variant="outlined">
                <CardContent>
                  <Typography className={classes.title} color="textSecondary" gutterBottom>
                    Muertes
                  </Typography>
                  <Typography className={classes.subTitle} color="textSecondary" gutterBottom>
                    {this.state.Muertes}
                  </Typography>
                </CardContent>
              </Card>
            </Grid>
          </Grid>
          <Grid
            container
            direction="row"
            justify="center"
            alignItems="stretch"
            className={classes.Mundo}
          >
            <Grid item xs={12} md={4} lg={4} style={{padding:'10px'}}> 
              <Card className={classes.root} variant="outlined">
                <CardContent>
                  <Typography className={classes.title} color="textSecondary" gutterBottom>
                    Confirmados Totales
                  </Typography>
                  <Typography className={classes.subTitle} color="textSecondary" gutterBottom>
                    
                    <NumberFormat value={this.state.ConfirmadosMundial} displayType={'text'} thousandSeparator={true}/>

                  </Typography>
                </CardContent>
              </Card>
            </Grid>
            <Grid item xs={12} md={4} lg={4} style={{padding:'10px'}}> 
              <Card className={classes.root} variant="outlined">
                <CardContent>
                  <Typography className={classes.title} color="textSecondary" gutterBottom>
                    Recuperados Totales
                  </Typography>
                  <Typography className={classes.subTitle} color="textSecondary" gutterBottom>
                    <NumberFormat value={this.state.RecuperadosMundial} displayType={'text'} thousandSeparator={true}/>
                    
                  </Typography>
                </CardContent>
              </Card>
            </Grid>
            <Grid item xs={12} md={4} lg={4} style={{padding:'10px'}}> 
              <Card className={classes.root} variant="outlined">
                <CardContent>
                  <Typography className={classes.title} color="textSecondary" gutterBottom>
                    Muertes Totales
                  </Typography>
                  <Typography className={classes.subTitle} color="textSecondary" gutterBottom>
                    <NumberFormat value={this.state.MuertesMundial} displayType={'text'} thousandSeparator={true} />
                    
                  </Typography>
                </CardContent>
              </Card>
            </Grid>
          </Grid>
          <Grid
            container
            direction="row"
            justify="center"
            alignItems="stretch"
            className={classes.Mundo}
          >
          <Typography className={classes.title} color="textSecondary" gutterBottom>
            {this.state.FechaActualizacion}
          </Typography>
          </Grid>
        </Grid>
      </Grid>
    );
  }
}





class Map extends React.Component {
  constructor(props) {
    super(props);
  
    this.state = {selectedCountries: {}};
  }
  componentDidMount(){
    console.log('Cambio')
    this.setState({selectedCountries:{[this.props.Pais]:true}})
  }
  toggleCountry = country => {
    const { selectedCountries } = this.state;
    this.props.CambiarPais(country.id);
    this.setState({
      selectedCountries: {
        [country.id]: true
      }
    });
  };
  render() {
    const { selectedCountries } = this.state;
    const mapCountries = country.map(country => (
      <path
        key={country.id}
        d={country.shape}
        style={{
          fill: selectedCountries[country.id] ? "tomato" : "#eee",
          cursor: "pointer",
          stroke: "#ccc"
        }}
        onClick={() => this.toggleCountry(country)}
      />
    ));
    return (
      <svg
        xmlns="http://www.w3.org/2000/svg"
        height="250"
        width="100%"
        viewBox="0 0 2000 1001"
      >
        {mapCountries}
      </svg>
    );
  }
}


export default withStyles(useStyles)(App);
